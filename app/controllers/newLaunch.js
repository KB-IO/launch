var fs = require('fs');



var {fadeIn, fadeOut, mkdir, lastUpdate, elmByID} = require('../middlewares/utils')
var { flowInfo, sendRequest } = require('../middlewares/enfocus_info');
var { relFilePath } = require('../middlewares/fileDropParsing');
var { writeFileTable } = require('../middlewares/fileDropParsing');
var { NEW_Launch_Insert, SUBMIT_Launch_Insert, SUBMIT_Files_Insert } = require('../db/sql');



var launchOverlay = document.getElementById("launch_overlay")
var launchBtn = document.getElementById("new_launch");
var launchNumberInput = document.getElementById('launch_number')

var folders = ['Customer_Files', 'Preflight'];


// File Hold;
var storedFiles = [];




if (ENV === 'development') {
    var PreflightNumber = '/Users/milton/Desktop/IO_Prepress/Preflights/PreflightNumber/PreflightNumber.txt';
    var Preflight_Directory = '/Users/milton/Desktop/IO_Prepress/Preflights/'
} else {
    var PreflightNumber = '/Volumes/IO_Prepress/Preflights/PreflightNumber/PreflightNumber.txt';
    var Preflight_Directory = '/Volumes/IO_Prepress/Preflights/';
}


elmByID("submit_launch").addEventListener("click", () => {
    submitLaunch(storedFiles, "launch_number", "launch_client_name", "printYesNo")
});

elmByID("cancel_launch").addEventListener("click", () => {
    cancelLaunch("launch_number", "launch_client_name", "printYesNo", "launch_overlay", "new_launch", "launchTable");

});



launchBtn.addEventListener("click", () => {
    // Create:
    //  - Launch Folder
    // Grab number
    // Place # in launch ID
    // Fade Out
    newLaunch(PreflightNumber, Preflight_Directory)
})



function newLaunch(path, PreflightDirectory) {
    var fld_ext = '_Preflight'
    var folders = ['Customer_Files', 'Preflight'];
    var Preflight_Directory = PreflightDirectory;
    fs.readFile(path, 'utf-8', (err, data) => {
        if (err) throw err;
        // Read data
        data = parseInt(data) + 1;
        // Write to file with incremented number
        fs.writeFile(path, data, (err) => {
            if (err) throw err;
        });
        var preflightFolder = data + fld_ext;
        folders.forEach((folder) => {
            var newFolder = Preflight_Directory + preflightFolder + '/' + folder
            mkdir(newFolder);
        });
        // Add New Launch to Database--Log
        NEW_Launch_Insert(IOGLOBALS['authEmail'], data);
        // Add launch number to screen
        launchNumberInput.innerText = data + fld_ext;
        fadeOut(launchOverlay);
        setTimeout(enableButtons, 2000);
        launchBtn.disabled = true;

    });
}


function enableButtons(){
    launchOverlay.style.display = 'none'
}



function launchDrop(e) {
    // Adds dropped files to array
    // Writes files to screen
    var files = filesObjToArray(e)
    files.forEach(function(f) {
        if (filenameInArray(storedFiles, f) === false) {
            writeFileTable(elmByID('launchTable'), f.name);
            storedFiles.push(f);
        }
    })
}


function filesObjToArray(e) {
    var filesList = e.target.files || e.dataTransfer.files;
    return Array.prototype.slice.call(filesList);
}

function filenameInArray(fileArray, file) {
    // Checks if file is in fileArray.
    // If YES, returns true
    // if NO, returns false
    return fileArray.some(fileArray => fileArray.name === file.name)
}

function setFilesToArray (files) {
    var files = filesObjToArray(files);
}

function cancelLaunch(removeElement, clientName,printSel, lOverlay, nLaunch, fileTable){
        // Removes files from array
        storedFiles.length = 0;
        elmByID(removeElement).innerText = '';
        elmByID(clientName).value = '';
        elmByID(printSel).value = 'YES';
        fadeIn(elmByID(lOverlay))
        elmByID(nLaunch).disabled = false;
        elmByID(fileTable).innerHTML = '';
}



function submitLaunch(files, launchNoElm, clientNameElm, toPrintElm) {
        var launchNo = elmByID(launchNoElm).innerText;
        var clientName = elmByID(clientNameElm).value;
        var toPrint = elmByID(toPrintElm).value;


        // Creating a new instance
        var data = new FormData();

        // Appending data to instance
        data.append('flowId', flowInfo("LAUNCH", "flowId"));
        data.append('objectId', flowInfo("LAUNCH", "objectId"));
        data.append('jobName', launchNo);



        // Add files to thingy...
        for (var i = 0; i < files.length; i++) {
            data.append('file[' + i + '][file]', files[i]);
            data.append('file[' + i + '][path]', relFilePath(files, i));
        }
        // End of appending files
        data.append('modified', lastUpdate());


        // Sends to Database for logging
        SUBMIT_Launch_Insert(clientName, files, IOGLOBALS['authEmail'], launchNo, toPrint, function(result){
            SUBMIT_Files_Insert(result[0], result[1], function (file_insert_res) {
                // Metadata
                data.append('metadata', JSON.stringify([{
                    "name": "IO-Launch Number",
                    "value": launchNo
                },
                    {
                        "name": "Client Name",
                        "value": clientName
                    },
                    {
                        "name": "Hidden - Routing XSLT - CSR",
                        "value": "CSR"
                    },
                    {
                        "name": "Send to Printer",
                        "value": toPrint
                    },
                    {
                        "name": "MetadataStuffing",
                        "value": createMeatadata(file_insert_res)
                    }
                ]));
                // End of Metadata

                if (ENV === 'production'){
                    // Send Request
                    sendRequest(data)
                }


            })
        });


        for (var b = 0; b < files.length; b++) {
            copyFile(files[b], Preflight_Directory, "launch_number")
        }

        cancelLaunch("launch_number", "launch_client_name", "printYesNo", "launch_overlay", "new_launch", "launchTable");
}

function copyFile(file, target, launchNo) {
    var launchNo = elmByID(launchNo).innerText;
    var initialFilePath = file.path;
    var destPath = target + launchNo + '/Customer_Files/' + file.name;
    fs.createReadStream(initialFilePath).pipe(fs.createWriteStream(destPath));
}


function createMeatadata(returnedFiles) {
    var tempObj = {}
    for (i = 0; i < returnedFiles.length; i++) {
        tempObj[returnedFiles[i].filename] = returnedFiles[i].file_id;
    }
    return JSON.stringify(tempObj);
}