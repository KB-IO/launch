var Emitter = require('events').EventEmitter;
var util = require('util');
var path = require('path');
var fs = require('fs');
var View = require('../middlewares/viewRouting');
// hotFolder
// auth

var App = function() {
    this.on("view-selected", function(viewName) {
        var view = new View(viewName);
        this.emit("rendered", view.Html);
    });
};

// EXPORTS
util.inherits(App, Emitter);
module.exports = new App();