var { truncate, elmByID, parseDate } = require('../middlewares/utils');
var { basePostflightHistory } = require('../db/sql');


basePostflightHistory(IOGLOBALS.userUUID, function(data){
    // hi = result
    //console.log(data);
    writeToBaseHistoryTable('page_history_table', data);

})



function writeToBaseHistoryTable(tblID, data) {
    let html = "";

    for (let i = 0; i < data.length; i++) {
        let jobNo, submission_date, clientName, filename, pieChart, file_id;

        file_id = data[i]['file_id'];

        jobNo = data[i]['job_no'];
        submission_date = parseDate(data[i]['date']);
        clientName = truncate(data[i]['client_name'],25);
        pieChart = ":)";
        filename = truncate(data[i]['filename'],50);
        file_id = data[i]['file_id'];

        html += '<tr>\
                        <td width="10px"></td>\
                        <td width="60px">' + jobNo +'</td>\
                        <td width="200px">' + submission_date + '</td>\
                        <td width="130px">' + clientName + '</td>\
                        <td width="45px">' + pieChart + '</td>\
                        <td width="550px"><a href="#" class="history_tbl" id="' + file_id + '"><span>' + filename + '</span></a></td>\
                   </tr>';
    }
    document.getElementById("page_history_table").innerHTML = html;
}


//
// function writeToBaseHistoryTable(tblID, data) {
//
//     let tab = document.getElementById('page_history_table')
//     for (let i = 0; i < data.length; i++) {
//         let jobNo, submission_date, clientName, filename, template, pieChart, hidden, file_id
//         file_id = data[i]['file_id'];
//
//         let row = document.createElement("tr");
//         let td_1 = document.createElement("td");
//         let td_2 = document.createElement("td");
//         let td_3 = document.createElement("td");
//         let td_4 = document.createElement("td");
//         let td_5 = document.createElement("td");
//         let td_6 = document.createElement("td")
//
//         hidden = document.createTextNode("");
//         jobNo = document.createTextNode(data[i]['job_no']);
//         submission_date = document.createTextNode(parseDate(data[i]['date']));
//         clientName = document.createTextNode(truncate(data[i]['client_name'],25));
//         //pieChart = document.createTextNode(`<svg class="peity" height="16" width="16"><path d="M 8 8 L 8 0 A 8 8 0 0 1 14.933563796318165 11.990700825968545 Z" fill="#1ab394"></path><path d="M 8 8 L 14.933563796318165 11.990700825968545 A 8 8 0 1 1 7.999999999999998 0 Z" fill="#d7d7d7"></path></svg>`);
//         pieChart = document.createTextNode(":)")
//         filename = document.createTextNode(truncate(data[i]['filename'],50));
//
//
//         //document.create
//         td_1.appendChild(hidden)
//         td_2.appendChild(jobNo)
//         td_3.appendChild(submission_date)
//         td_4.appendChild(clientName)
//         td_5.appendChild(pieChart)
//         td_6.appendChild(filename)
//
//         row.appendChild(td_1).setAttribute("width", "10")
//         row.appendChild(td_2).setAttribute("width", "60")
//         row.appendChild(td_3).setAttribute("width", "200")
//         row.appendChild(td_4).setAttribute("width", "130")
//         row.appendChild(td_5).setAttribute("width", "45")
//         row.appendChild(td_6).setAttribute("data-uuid", file_id);//.setAttribute("width", "550")
//
//         tab.appendChild(row)
//     };
//
// }
