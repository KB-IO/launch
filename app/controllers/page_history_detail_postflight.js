var { truncate, elmByID, parseDate } = require('../middlewares/utils');
var {parseSizes, cleanEffectiveRes} = require('../middlewares/parsingDetails');
var {basePostflightDetails} = require('../db/sql');




function queryWrite(uuid){
    basePostflightDetails(uuid, function(result){
        console.log(result[0].get_postflight_details);
        writeTables(result[0].get_postflight_details)

    } )
}
function writeTables(info){
    writeFilename(info);
    clientName(info);
    jobNumber(info);
    writePageSizeTable(info);
    writeColorTable(info);
    writeProductionNotes(info);
    writeResolutionTable(info);
    writeOtherFiles(info);

}



function clientName (data) {
    let tab = document.getElementById('postflight_detail_clientName');
    tab.innerText = data['submit_details'][0]['client_name'];
}

function jobNumber(data){
    let tab = document.getElementById('postflight_detail_jobNumber');
    tab.innerText= data['submit_details'][0]['job_no']

}

function bagNumber(data){
    let tab = document.getElementById('postflight_detail_bagNumber');
    tab.innerText = data['submit_details'][0]['bag_no']
}
function writeProductionNotes(data) {
    let tab = document.getElementById('detail_postflight_prod_notes');
    tab.value = data['submit_details'][0]['prod_notes'];
}

function writeFilename(data){
    let tab = document.getElementById('detail_postflight_filename');
    tab.innerText = truncate(data['filename'],45);
}






function writeOtherFiles(data){
    let html= "";

    let tab = document.getElementById('details_postflight_other_submitted_files_table');

    data = data["other_files"];

    if (data.length === 0) {
        // TODO: have it write: "NO COLOR DATA AVAILABLE"
    }

    // if we have data > 1
    for (let i = 0; i < data.length; i++){
        let posNum, filename, file_id;

        posNum = i + 1;
        filename = data[i]['filename'];
        file_id = data[i]['file_id'];

        console.log(posNum, filename, file_id)
        html += '<tr>\
                    <td width="30">' + posNum + '</td>\
                    <td width="230"><a href="#" class="history_tbl" id="' + file_id + '"><span>' + filename +'</td>\
                </tr>';
    }
    document.getElementById('detail_postflight_other_submitted_files_table').innerHTML = html;
}
// -------------------------------------------
/// EVERYTHING BELOW is for the tabbed tables...
// function writeOtherFiles(data){
//     let tab = document.getElementById('detail_postflight_other_submitted_files_table');
//     data = data['other_files'];
//     if (data.length === 0) {
//         // TODO: Have it write "NO COLOR DATA AVAILABLE
//     }
//     for (let i = 0; i < data.length; i++) {
//
//         let posNum, filename, file_id;
//
//         file_id = data[i]['file_id'];
//
//         let row = document.createElement("tr");
//         let td_1 = document.createElement("td");
//         let td_2 = document.createElement("td");
//
//         posNum = document.createTextNode(i + 1);
//         filename = document.createTextNode(truncate(data[i]['filename'],60));
//
//         td_1.appendChild(posNum);
//         td_2.appendChild(filename)
//
//         row.appendChild(td_1).setAttribute("width", "30")
//         row.appendChild(td_2).setAttribute("width", "230")
//
//         tab.appendChild(row).setAttribute("data-uuid", file_id);
//
//     }
//
//
// }
function writeColorTable(data){
    let tab = document.getElementById('detail_postflight_color_table');
    data = data['file_colors'];
    if (data.length === 0) {
        // TODO: Have it write "NO COLOR DATA AVAILABLE
    }
    for (let i = 0; i < data.length; i++) {

        let posNum, pageNum, colorMsg, hidden;

        let row = document.createElement("tr");
        let td_1 = document.createElement("td");
        let td_2 = document.createElement("td");
        let td_3 = document.createElement("td");
        let td_4 = document.createElement("td");

        hidden = document.createTextNode("");
        posNum = document.createTextNode(i + 1);
        pageNum = document.createTextNode(data[i]['page']);
        colorMsg = document.createTextNode(data[i]['message'])

        td_1.appendChild(hidden);
        td_2.appendChild(posNum);
        td_3.appendChild(pageNum);
        td_4.appendChild(colorMsg);

        row.appendChild(td_1).setAttribute("width", "5")
        row.appendChild(td_2).setAttribute("width", "50")
        row.appendChild(td_3).setAttribute("width", "50")
        row.appendChild(td_4).setAttribute("width", "120")

        tab.appendChild(row);
    }
}



function writePageSizeTable(data){
    let tab = document.getElementById('detail_postflight_page_size_table');
    data = data['file_page_sizes'];
    if (data.length === 0) {
        // TODO: Have it write "NO COLOR DATA AVAILABLE
    }
    for (let i = 0; i < data.length; i++) {

        let pageNum, trimBox, bleedBox, artBox, mediaBox, cropBox;

        let row = document.createElement("tr");
        let td_1 = document.createElement("td");
        let td_2 = document.createElement("td");
        let td_3 = document.createElement("td");
        let td_4 = document.createElement("td");
        let td_5 = document.createElement("td");
        let td_6 = document.createElement("td");

        pageNum = document.createTextNode(data[i]['page_num']);
        trimBox = document.createTextNode(parseSizes(data[i]['trim_box']));
        bleedBox = document.createTextNode(parseSizes(data[i]['bleed_box']));
        artBox = document.createTextNode(parseSizes(data[i]['art_box']));
        mediaBox = document.createTextNode(parseSizes(data[i]['media_box']));
        cropBox = document.createTextNode(parseSizes(data[i]['crop_box']));

        td_1.appendChild(pageNum);
        td_2.appendChild(trimBox);
        td_3.appendChild(bleedBox);
        td_4.appendChild(artBox);
        td_5.appendChild(mediaBox);
        td_6.appendChild(cropBox);

        row.appendChild(td_1).setAttribute("width", "50")
        row.appendChild(td_2).setAttribute("width", "90")
        row.appendChild(td_3).setAttribute("width", "90")
        row.appendChild(td_4).setAttribute("width", "90")
        row.appendChild(td_5).setAttribute("width", "90")
        row.appendChild(td_6).setAttribute("width", "90")

        tab.appendChild(row);
    }
}




function writeResolutionTable(data){
    let tab = document.getElementById('detail_postflight_resolution_table');
    data = data['file_resolutions'];
    if (data.length === 0) {
        // TODO: Have it write "NO RESOLUTION DATA AVAILABLE
    }
    for (let i = 0; i < data.length; i++) {

        let posNum, pageNum, resolution, hidden;

        let row = document.createElement("tr");
        let td_1 = document.createElement("td");
        let td_2 = document.createElement("td");
        let td_3 = document.createElement("td");
        let td_4 = document.createElement("td");

        hidden = document.createTextNode("");
        posNum = document.createTextNode(i + 1);
        pageNum = document.createTextNode(data[i]['page_num']);
        resolution = document.createTextNode(cleanEffectiveRes(data[i]['effective_resolution']))

        td_1.appendChild(hidden);
        td_2.appendChild(posNum);
        td_3.appendChild(pageNum);
        td_4.appendChild(resolution);

        row.appendChild(td_1).setAttribute("width", "5")
        row.appendChild(td_2).setAttribute("width", "50")
        row.appendChild(td_3).setAttribute("width", "50")
        row.appendChild(td_4).setAttribute("width", "120")

        tab.appendChild(row);
    }
}