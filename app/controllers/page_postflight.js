var {flowInfo, sendRequest} = require('../middlewares/enfocus_info');
var {clientName, wrongSubmitPoint, JobNumber, bagNumber, relFilePath, writeFileTable} = require('../middlewares/fileDropParsing');
var {lastUpdate} = require('../middlewares/utils')
var {writeProductionNote} = require('../middlewares/productionNote')
var { SUBMIT_Postflight_Insert, SUBMIT_Files_Insert} = require('../db/sql')


// File Hold;
var storedFiles = [];


// EVENT LISTNERS
document.getElementById('cancel_postflight').addEventListener("click", clearPostflightData);


// Submit Postflight
var postflightSubmitButton = document.getElementById('submit_postflight');
postflightSubmitButton.addEventListener("click", () => {
    var tBoxPage = document.getElementById('postflight-prodNotes');
    var tBoxPageChkBox = document.getElementById("postflight-prodNotes-checkbox");
    var mBoxPage = document.getElementById('productionNotesModal');
    var mBoxSubmit = document.getElementById('submit_postflightFromModal');


    var clientNameBox = document.getElementById('postflight_clientName');
    var jobNumBox = document.getElementById('postflight_jobNumber');
    var bagNumBox = document.getElementById('postflight_bagNumber');

    if (tBoxPage.value.length == 0 ){  // TODO: NEED TO REFACTOR: && tBoxPageChkBox.checked !== true) {
        // TODO: Create module global that will have all selectors available to it. Need to clean this shit up!
        alert("Please add your Production Notes");

    } else if (tBoxPage.value.length > 0) {
        SUBMIT_Postflight_Insert(clientNameBox.innerText, storedFiles, IOGLOBALS['authEmail'], jobNumBox.innerText, tBoxPage.value, bagNumBox.innerText, function(result) {
            SUBMIT_Files_Insert(result[0], result[1], function(file_insert_res) {
                submitPostflight(storedFiles, tBoxPage, clientNameBox, jobNumBox, bagNumBox, tBoxPageChkBox, createMeatadata(file_insert_res));
                clearPostflightData();
            })
        });
    }
});





function handleDropFiles(e) {
    // Adds dropped files to array
    // Writes files to screen
    // Alerts user if submitting from wrong directory
    var filesList = e.target.files || e.dataTransfer.files;
    var files = Array.prototype.slice.call(filesList);

    if (wrongSubmitPoint(files) === "approved") {
        // If storedFiles is currently empty
        if (storedFiles.length < 1) {
            enablePostflightFeatures()
            writeToScreenNonFile(files)
        }

        var postflightFileTable = document.getElementById('postflightTable');
        files.forEach(function(f, i){
            // Checks for f.name in the file array. If not in array, add to array.
            if (storedFiles.some(storedFiles => storedFiles.name === f.name) === false) {
                writeFileTable(postflightFileTable, f.name);
                storedFiles.push(f);
            }
        });
    } else {
        alert("You're attempting to submit files from the wrong directory")
    }

}

function writeToScreenNonFile(files) {
    var clientNameBox = document.getElementById('postflight_clientName');
    var jobNumBox = document.getElementById('postflight_jobNumber');
    var bagNumBox = document.getElementById('postflight_bagNumber');

    clientNameBox.innerText = clientName(files, 0);
    jobNumBox.innerText = JobNumber(files, 0);
    bagNumBox.innerText = bagNumber(files,0);

}

function enablePostflightFeatures(){
    document.getElementById('postflight-prodNotes').disabled = false;
    document.getElementById('cancel_postflight').disabled = false;
    document.getElementById('submit_postflight').disabled = false;
    document.getElementById('postflight-prodNotes-checkbox').disabled = false;
}

function clearPostflightData() {
    var clientNameBox = document.getElementById('postflight_clientName');
    var jobNumBox = document.getElementById('postflight_jobNumber');
    var bagNumBox = document.getElementById('postflight_bagNumber');
    var fileTable = document.getElementById('postflightTable');
    var prodNotes = document.getElementById('postflight-prodNotes');
    var checkBox = document.getElementById('postflight-prodNotes-checkbox');

    // Clearing things / Deleting things
    clientNameBox.innerText = '';
    jobNumBox.innerText = '';
    bagNumBox.innerText = '';
    fileTable.innerHTML = '';
    prodNotes.value = '';
    console.log("Clicked")
    document.getElementById('cancel_postflight').disabled = true;
    document.getElementById('submit_postflight').disabled = true;
    prodNotes.disabled = true;
    storedFiles.length = 0;
    checkBox.disabled = true;
    checkBox.checked = false;
}


function submitPostflight(files, textBox, clientNameBox, jobNumBox, bagNumBox, checkBox,submitted_files) {
    var prodNotes = textBox.value;
    // TODO: Add those to formData
    // TODO: Submit?
    var jobNumberValue = jobNumBox.innerText;
    var clientNameValue = clientNameBox.innerText;
    var bagNumberValue = bagNumBox.innerText;
    var checkBoxValue = checkBox.checked;

    // Creating new FormData
    var data = new FormData();
    // Appending data to formData
    data.append('flowId', flowInfo("POSTFLIGHT", "flowId")); // 46 (POSTFLIGHT)
    data.append('objectId', flowInfo("POSTFLIGHT", "objectId")); // 286 (POSTFLIGHT)
    data.append('jobName', jobNumberValue + "-" + clientNameValue);
    data.append('metadata', JSON.stringify(
        [
            { "name": "Job #", "value": jobNumberValue },
            { "name": "Client Name", "value": clientNameValue },
            { "name": "Hidden - Routing XSLT - Prepress", "value": "Prepress" },
            { "name": "Bag No", "value": bagNumberValue },
            { "name": "MetadataStuffing", "value": submitted_files }
        ]));
    for (var i = 0; i < files.length; i++) {
        data.append('file[' + i + '][file]', files[i]);
        data.append('file[' + i + '][path]', relFilePath(files, i));
    }
    data.append('modified', lastUpdate());
    // WriteProductionNotes to the production folder
    // if folder isn't there, will make one.
    // TODO: Write production Note function to make folder only if not created
    sendRequest(data);
    if (checkBoxValue === false) {
        // if checkbox is not selected, write production note.
        writeProductionNote(IOGLOBALS["authEmail"], prodNotes, clientNameValue, jobNumberValue, bagNumberValue, files);
    }

    // Send request

}

function skipNotesCheckbox(checkbox='postflight-prodNotes-checkbox', prodNoteBox='postflight-prodNotes') {
    var textBox = document.getElementById(prodNoteBox);
    if (document.getElementById(checkbox).checked) {
        textBox.disabled = true;
        textBox.value = 'User selected to skip notes';
    } else {
        textBox.disabled = false;
        textBox.value = '';
    }
}

function createMeatadata(returnedFiles) {
    var tempObj = {}
    for (i = 0; i < returnedFiles.length; i++) {
        tempObj[returnedFiles[i].filename] = returnedFiles[i].file_id;
    }
    return JSON.stringify(tempObj);
}

