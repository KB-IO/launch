const db = require('../db/db');

function LOG_Auth_Insert(userEmail, authEvent, cb){
    // EventType for login is 1
    var eventType = {
        LOGIN: 1,
        LOGOUT: 2,
        ERROR: 3,
        QUIT: 4,
        TIMEOUT: 5
    }
    db.query(`
    INSERT INTO log_user_auth (app_user_fk, event_type_fk)
    VALUES ((SELECT app_user_id
             FROM app_user
             WHERE email = $1), $2) RETURNING app_user_fk`,

        [userEmail.toLowerCase(), eventType[authEvent]], (err,res) => {
            if(err) throw err;
            else{
                if (authEvent === 'LOGIN') {
                    cb(res.rows[0].app_user_fk);
                }

            }
        });

}

function NEW_Launch_Insert(userEmail, launchNum){
    db.query(`
        INSERT INTO launch_create (user_fk, launch_num)
        VALUES ((SELECT app_user_id
             FROM app_user
             WHERE email = $1), $2)`,

        [userEmail.toLowerCase(), launchNum], (err) => {
        if (err) throw err;
        });
}


function SUBMIT_Postflight_Insert(clientName, files, userEmail, jobNum, prodNote, BagNo,callback) {
    var forSubmitFiles = files.slice();
    db.query(`INSERT INTO postflight_submit (client_name, file_count, app_user_fk, job_no, prod_notes, bag_no)
                VALUES ($1, $2, (SELECT app_user_id
                                 FROM app_user
                                 WHERE email = $3), $4, $5, $6) RETURNING submit_id`,
        [clientName.toString().toLowerCase(), parseInt(files.length), userEmail.toLowerCase(), parseInt(jobNum), prodNote, BagNo], (err, res) => {
            if (err) throw err;
            else{
                var newSubmit = [res.rows[0].submit_id, forSubmitFiles]
                callback(newSubmit);
                // Why did I comment this out?
                //callback(newSubmitID, forSubmitFiles)
            }
        });
}


function SUBMIT_Launch_Insert(client_name, files, userEmail, launchNum, print,cb) {
    var forSubmitFiles = files.slice();
    //TODO: CHANGE THIS SO I CAN ADD FILES TO THE OTHER TABLE!!!
    db.query(`INSERT INTO launch_submit (client_name, file_count, app_user_fk, launch_num_fk, print)
                VALUES ($1, $2, (
                    SELECT app_user_id
                    FROM app_user
                    WHERE email = $3), (
                        SELECT launch_id
                        FROM launch_create
                        WHERE launch_num = $4), $5) RETURNING submit_id`,
        [client_name.toLowerCase(), parseInt(files.length), userEmail.toLowerCase(), parseInt(launchNum.split("_")[0]), print.toLowerCase()], (err, res) => {
            if (err) throw err;
            else {
                var newSubmit = [res.rows[0].submit_id, forSubmitFiles]
                cb(newSubmit)
            }
        });
}

function SUBMIT_Files_Insert(newSubmitID, files,callback) {
    let rows = transformObj(newSubmitID, files);
    db.query(buildStatement(`INSERT INTO submitted_file (submit_fk, filename) VALUES `, rows,['filename', 'file_id']), (err,res) => {
        if (err) throw err;
        callback(res.rows)

    })
}


// For... umm... Get POSTFLIGHT History
function basePostflightHistory (userUUID, callback) {
    db.query(`SELECT * FROM get_postflight_by_user_uuid($1)`, [userUUID], (err, res) => {
        if (err) throw err;
        else {
            callback(res.rows);
        }
    })
}

function basePostflightDetails(fileUUID, callback) {
    db.query(`SELECT get_postflight_details($1)`, [fileUUID], (err, res) =>{
        if (err) throw err;
        else {
            callback(res.rows);
        }
    })
}



 ////////////////////////
 // NON SQL Functions //
///////////////////////
function buildStatement (insert, rows, returnFieldArr=[]) {
    const params = []
    const chunks = []
    // If returnFieldArr is not null, we add params to allow returning
    // fields that are listed in the array. If null, returning statement isn't added.
    let returning = ''
    if (returnFieldArr.length !== 0) {
        returning = ' RETURNING ' + returnFieldArr.toString();
    }
    rows.forEach(row => {
        const valueClause = []
        Object.keys(row).forEach(p => {
            params.push(row[p])
            valueClause.push('$' + params.length)
        })
        chunks.push('(' + valueClause.join(', ') + ')')
    })
    return {
        text: insert + chunks.join(', ') + returning,
        values: params
    }
}


function transformObj (newSubmitID, files){
    // Returns an array of objects.
    // newSubmitID is static in each object
    // files are dynamic in; each object contains the name of that file at position 'i' in array.
    var rows = [];
    for (var i = 0; i < files.length; i++) {
        rows.push({
            submit_fk: newSubmitID,
            filename: files[i].name
        })
    }
    return rows;
}




module.exports = {LOG_Auth_Insert, NEW_Launch_Insert, SUBMIT_Launch_Insert, SUBMIT_Postflight_Insert, SUBMIT_Files_Insert, basePostflightHistory,
                  basePostflightDetails};

// RESOURCES:
// Return after insert: https://github.com/brianc/node-postgres/wiki/FAQ
// Multi-Row Insert: https://github.com/brianc/node-postgres/issues/530