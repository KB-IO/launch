const { Pool, end} = require('pg');

// OKAY... This thing actually logs you off after 30 seconds. This is good. It recycles the connections.

// var connection = {
//     user: 'postgres',
//     database: 'test', // 'launch' is prod; 'testing' is dev
//     password: '!!ImageOptions',
//     host: '172.24.5.130',
//     port: 5432,
//     max: 10, // Max number of clients in the pool
//     idleTimeoutMillis: 30000, // How long a client is allowed to remain idle before being closed
// };
var connection = {
    user: 'postgres',
    database: 'launch', // 'launch' is prod; 'testing' is dev
    password: '!!ImageOptions',
    host: '172.24.5.130',
    port: 5432,
    // Note: This may change in the future. Depends on how I get data for the History / Dashboard
    max: 2, // Max number of clients in the pool
    idleTimeoutMillis: 5000, // How long a client is allowed to remain idle before being closed
};




// This initializes a connection pool
// It will keep idle connections open for 30 secodns
// and set a limit of maximum 10 idle clients
const pool = new Pool(connection);

pool.on('error', function(err, client) {
    console.error('idle client error', err.message, err.stack);
});

// Export the query method for passing queries to the pool
module.exports.query = function(text, values, callback) {
    console.log('query:', text, values);
    return pool.query(text, values, callback);
};

// The pool also supports checking out a client
// for multiple operations, such as a transaction.
module.exports.connect = function(callback) {
    return pool.connect(callback);
};