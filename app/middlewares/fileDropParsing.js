var fs = require('fs');
var pad = require('./utils').pad


// Read files inside directory
// https://stackoverflow.com/questions/2727167/how-do-you-get-a-list-of-the-names-of-all-files-present-in-a-directory-in-node-j

function wrongSubmitPoint(files) {
    // Placing [0] since there should always be at least one file; index 0
    var pathArray = filePath(files, 0).split("/");
    var PrintReadyIndex = pathArray.indexOf("Print_Ready");
    var CutFilesIndex = pathArray.indexOf("Cut_Files");
    // Checking filepath to see if above is there
    if (PrintReadyIndex > -1 || CutFilesIndex > -1) {
        return "approved"
    } else {
        return "denied"
    }
}

function filePath(files, i) {
    return files[i].path;
}

function relFilePath(files, i) {
    var fSplit = files[i].path.split("/");
    return fSplit[fSplit.length - 2] + '/' + files[i].name;
}


function clientName(files, i) {
    // Returns the client name
    // Note: Keep this outside of the loop
    // TODO: Build function where it sits inside the loop, collects all client names, compares all
    //       and then if all are same, make distinct then store data
    var pathArray = filePath(files, i).split("/");
    var PrintReadyIndex = pathArray.indexOf("Print_Ready");
    var CutFilesIndex = pathArray.indexOf("Cut_Files");

    if (PrintReadyIndex > -1) {
        var clientNameIndex = PrintReadyIndex + 1;
        var startNameIndex = pathArray[clientNameIndex].indexOf("_") + 1;
        return pathArray[clientNameIndex].slice(startNameIndex);
    } else {
        var clientNameIndex = CutFilesIndex + 1;
        var startNameIndex = pathArray[clientNameIndex].indexOf("_") + 1;
        return pathArray[clientNameIndex].slice(startNameIndex);
    }
}

function JobNumber(files, i) {
    // Returns the job number
    // Note: Keep this outside of the loop
    // TODO: Build function where it sits inside the loop, collects all job numbers, compares all
    //       and then if all are same, make distinct then store data
    var pathArray = filePath(files, i).split("/");
    var PrintReadyIndex = pathArray.indexOf("Print_Ready");
    var CutFilesIndex = pathArray.indexOf("Cut_Files");

    if (PrintReadyIndex > -1) {
        var jobNumberIndex = PrintReadyIndex + 1;
        return pathArray[jobNumberIndex].split("_")[0];
    } else {
        var jobNumberIndex = CutFilesIndex + 1;
        return pathArray[jobNumberIndex].split("_")[0];
    }
}

function bagNumber(files, i) {
    // Returns the bag number
    // NOTE: keep this outside the loop.

    // BAG NUMBER HAS A BUG!
    // TODO: REVIEW: 90240 IKEA. there is no bag #, yet it's coming out as 90240010021202
    // PARSING ERROR
    var pathArray = filePath(files, i).split("/");
    var PrintReadyIndex = pathArray.indexOf("Print_Ready");
    var CutFilesIndex = pathArray.indexOf("Cut_Files");

    if (PrintReadyIndex > -1) {
        var bagNumIndex = PrintReadyIndex + 2;
        var startBagIndex = pathArray[bagNumIndex].toLowerCase().indexOf("bag");
        if (startBagIndex > -1) {
            var cleanBagNum = pathArray[bagNumIndex].replace(/\D/g,'');
            var bagNum = pad(cleanBagNum, 99);
            return bagNum;
        } else {
            return "";
        };
    } else {
        var bagNumIndex = CutFilesIndex + 2;
        var startBagIndex = pathArray[bagNumIndex].toLowerCase().indexOf("bag");
        if (startBagIndex > -1) {
            var cleanBagNum = pathArray[bagNumIndex].replace(/\D/g,'');
            var bagNum = pad(cleanBagNum, 99);
            return bagNum;
        } else {
            return ""
        }
    }
}

function writeFileTable (element, filename) {
    // Create nodes?
    // TODO: Modify this to just needing to pass element id name as string to this function & change name.
    var newTR = document.createElement("tr");
    var childToTR = document.createElement("td");
    var fName = document.createTextNode(filename);

    // Appending
    childToTR.appendChild(fName);
    newTR.appendChild(childToTR);

    element.appendChild(newTR);
}

module.exports = {bagNumber, JobNumber, clientName, wrongSubmitPoint, relFilePath, writeFileTable};