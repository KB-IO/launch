
const remote = require('electron').remote;
var {LOG_Auth_Insert} = require('../db/sql');
var ENV = process.env.NODE_ENV;

var IOGLOBALS = {
    testObj: {
        success: true,
        user: "test@io.net",
        token: "b767c71faba9ff7b54e530a0cbf996ddb315131d",
        jobClientAccess: true,
        messagesAccess: true
    },
    authBearer: '',
    authEmail: '',
    userUUID: ''
};







setInterval(function() {
    // Every two seconds while the program is running, we are checking for a stored cookie
    // that represents authorized access to this program.
    // If cookie isn't found, display login screen
    var navElmWidth = document.getElementById('myNav').getBoundingClientRect()['width'];
    if (navElmWidth == 0) {
        checkAuthCookie()
    }
}, 2000);


function appLogin() {

    var emailInput = document.getElementById("authEmailInput").value;
    if (emailInput.indexOf("@") > -1) {
        //TODO:AUTH: Uncomment this so it will work in production

        var jsonString = getToken(emailInput);

        if (JSON.parse(jsonString)['success'] === true) {

            LOG_Auth_Insert(emailInput, 'LOGIN', function(cb){

                jsonString = JSON.parse(jsonString)
                jsonString['userUUID'] = cb.toString();
                jsonString = JSON.stringify(jsonString);
                setAuthCookie(jsonString, setExpiration(9));
                document.getElementById("authEmailInput").value = '';
                loggedInUser = JSON.parse(jsonString)['user'];
                console.log(loggedInUser);
                document.getElementById("LoggedInUser").innerHTML = loggedInUser;
                setGlobalBearerAndEmail()
                closeNav();
            });


        } else {
            document.getElementById('WrongEmail').innerHTML = '<h3 style="color: darkred">PLEASE ENTER YOUR CORRECT EMAIL ADDRESS</h3>'
            document.getElementById('authEmailInput').value = '';
            appLogin()
        }

    } else {
        document.getElementById('WrongEmail').innerHTML = '<h3 style="color: darkred">PLEASE ENTER YOUR CORRECT EMAIL ADDRESS</h3>'
        document.getElementById('authEmailInput').value = '';
    }
}


function appLogout() {
    removeCookie('http://postflight.com', 'authApp');
    // Removes user email from top-right nav
    document.getElementById("LoggedInUser").innerHTML = '';

    var userLogout = IOGLOBALS["authEmail"]
    // Removes BEARER Token from GLOBAL
    IOGLOBALS["authBearer"] = '';
    IOGLOBALS["authEmail"] = '';
    IOGLOBALS["userUUID"] = '';

    LOG_Auth_Insert(userLogout, "LOGOUT");
    // Opens the login screen
    openNav();
}



// Utility functions
function setExpiration(h = 8) {
    // Sets the time for expiration of the cookie to 'h' hours from now
    var myDate = new Date();
    // TODO: CHANGE TO (h * 60 * 60) Right now, it's currently set for minutes instead of hours
    return myDate.setTime((myDate.getTime() / 1000) + (h * 60 * 60));
}

function getToken(uLogin) {

    // Blank Password RSA hash
    var RSA_hash = "!%40%24QBk%252BmdP1YJcU2cf61053cbstHmB7ysxyFB2Oy63WZwtSWN%252FhJzfVuiYo%252Fn%252Fx2Xn4x7f18%252BOB8oc1jovP%252FkNqMjj2e5o3XZYGWE7Qp6%252BaREcnu4XbFuooJ3y0lKPfC%252B0oDGq1P368uFdB7PznwhtOEKLUCl1CrNz98zzt1avqXgY%253D";
    var data = JSON.stringify({ "username": uLogin, "password": RSA_hash });
    var xhr = new XMLHttpRequest();

    xhr.open("POST", "http://enfocusvm:51088/login", false);
    //xhr.open("POST", "http://0.0.0.0:8000/", false);

    xhr.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
    //console.log(data);
    xhr.send(data);
    console.log(xhr.responseText)
    return xhr.responseText;
}


///////////////////////////////////////
//////////////////////////////////////
//         COOKIE FUNCTIONS        //
////////////////////////////////////
function setAuthCookie(jsonString, expireDate) {
    // This sets the auth cookie
    remote.session.defaultSession.cookies.set({
        url: "http://postflight.com",
        name: "authApp",
        value: jsonString,
        expirationDate: expireDate
    }, (error, cookies) => {

    });
}

function removeCookie(url, name) {
    remote.session.defaultSession.cookies.remove(url, name, (error, cookie) => {
        console.log("Cookie - [REMOVED]\n Name: " + name + "\n Domain: " + url);
    })
}

function removeAllCookies() {
    remote.session.defaultSession.clearStorageData([], function(data) {
        console.log(data);
    })
}

function getAuthUserEmail() {
    remote.session.defaultSession.cookies.get({ url: 'http://postflight.com', name: 'authApp' }, (error, cookies) => {
        console.log(JSON.parse(cookies[0].value).user)
        return cookies

    });
};

// TODO: REMOVE THIS SHIT AND UNDERSTAND CALLBACKS

function checkAuthCookie() {
    remote.session.defaultSession.cookies.get({ url: 'http://postflight.com', name: 'authApp' }, (error, cookies) => {
        if (cookies.length == 0) {
            appLogout()
        }
    })
}


function setGlobalBearerAndEmail() {
    remote.session.defaultSession.cookies.get({ url: 'http://postflight.com', name: 'authApp' }, (error, cookies) => {
        if (cookies.length == 0) {
            setGlobalBearerAndEmail()

        } else if (cookies.length == 1) {
            var cookieValue = JSON.parse(cookies[0]["value"]);
            IOGLOBALS["authBearer"] = cookieValue["token"];
            IOGLOBALS["authEmail"] = cookieValue["user"]
            IOGLOBALS["userUUID"] = cookieValue["userUUID"]
            document.getElementById("LoggedInUser").innerHTML = cookieValue["user"]
            //console.log(getBearer)
        }
    });
}
// Already in the global namespace since I used script src.... in index.html
//module.exports = {IOGLOBALS};