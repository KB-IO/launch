var fs = require('fs');
var path = require('path');



function arrayTrim(split_Array) {
    return split_Array.map(Function.prototype.call.bind(String.prototype.trim));
};

function pad(value, length) {
    var length = length.toString().length;
    return (value.toString().length < length) ? pad("0" + value, length) : value.toString();
}


function currentDate() {
    var today = new Date();
    return arrayTrim(today.toLocaleString("en-US").split(","))[0];
}
function currentTime() {
    var today = new Date();
    return arrayTrim(today.toLocaleString("en-US").split(","))[1];
}

function ensureDirectoryExists(filePath) {
    var dirname = path.dirname(filePath);
    if (fs.existsSync(dirname)) {
        return true;
    }
    ensureDirectoryExists(dirname);
    fs.mkdirSync(dirname);
}

function lastUpdate() {
    var date = new Date();
    date.setDate(date.getDate());
    return date.toISOString();
}

// fade out


function fadeOut(el) {
    return new Promise(function (resolve, reject) {
        let opacity = 1;
        function fade(){
            if ((opacity -= .02) > 0){
                el.style.opacity = opacity;
                requestAnimationFrame(fade);
            } else {
                resolve();
            }
        }
        fade();
    });
};

function elmByID(elementID) {
    return document.getElementById(elementID);
}
// fade in

function fadeIn(el, display){
    el.style.opacity = 0;
    el.style.display = display || "block";

    (function fade() {
        var val = parseFloat(el.style.opacity);
        if (!((val += .02) > .8)) {
            el.style.opacity = val;
            requestAnimationFrame(fade);
        }
    })();
}

function mkdir(dir) {
    // we explicitly don't use `path.sep` to have it platform independent;
    var sep = '/';

    var segments = dir.split(sep);
    var current = '';
    var i = 0;

    while (i < segments.length) {
        current = current + sep + segments[i];
        try {
            fs.statSync(current);
        } catch (e) {
            fs.mkdirSync(current);
        }

        i++;
    }
}


function truncate(strValue, maxLen) {
    // Used to conform to maxium widths on the table so it doesn't distort.
    let strFrt, strBck, toCut;
    // Only do something if maxLen is smaller than strValue
    if (maxLen <= strValue.length) {
        toCut = Math.floor((maxLen - 3) / 2)
        strFrt = strValue.slice(0, toCut);
        strBck = strValue.slice(-toCut, strValue.length);
        return strFrt + '...' + strBck;
    } else {
        return strValue;
    }
}



function parseDate(dateStr) {
    return dateStr.getMonth() + '/' + dateStr.getDate() + '/' + (dateStr.getFullYear()).toString().slice(-2)
}
module.exports = {
    arrayTrim,
    pad,
    currentDate,
    currentTime,
    ensureDirectoryExists,
    lastUpdate,
    fadeIn,
    fadeOut,
    mkdir,
    truncate,
    elmByID,
    parseDate
};