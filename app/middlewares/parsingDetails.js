var {arrayTrim} = require('./utils')


function parseSizes(size) {
    var size;
    if (size.length === 0) {
        size = "Not Available";
    } else {
        var sClean = arrayTrim(size);
        // Calculating some stuff
        var gWidth = +(parseFloat(sClean[2]) - parseFloat(sClean[0])).toFixed(3);
        var gHeight = +(parseFloat(sClean[3]) - parseFloat(sClean[1])).toFixed(3);
        size = gWidth + '" x ' + gHeight + '"';
    };
    return size;
}

function cleanEffectiveRes(resolution){
    let clean = resolution.replace('(','').replace(')','').split(',');
    let x = parseFloat(clean[0]).toFixed(0);
    let y = parseFloat(clean[1]).toFixed(0);
    return '[' + x + ' x ' + y + '] ppi';
}
module.exports = {
    parseSizes,
    cleanEffectiveRes
}