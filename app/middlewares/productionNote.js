var fs = require('fs');
var path = require('path');
var utils = require('./utils');
let ENV = process.env.NODE_ENV;


function writeProductionNote(operator, userNotes, clientName, JobNumber, bagNumber, files) {
    // TODO: ADD IF BAG, CREATE WITH BAG#, IF NOT, STANDARD!
    var filename;
    if (bagNumber.length > 0){
        filename = "Production_Notes_BagNo_" + bagNumber + ".txt";
    } else {
        filename = "Production_Notes.txt";
    }


    if (ENV === 'development') {
        var filePath = '/Users/milton/Desktop/untitled\ folder/FileSubmitTests/Volumes/IO_Prepress/Production/'  + JobNumber + "_" + clientName + "/0 - Production Notes/" + filename;
    } else {
        var filePath = "/Volumes/IO_Prepress/Production/" + JobNumber + "_" + clientName + "/0 - Production Notes/" + filename;
    }

    // Checks if directory exists, if not, create. IF, leave alone
    utils.ensureDirectoryExists(filePath);
    // Creates file; Does not overwrite if file exists.
    var stream = fs.createWriteStream(filePath, { flags: 'a' });
    var notes = userNotes;
    // Create text for files submitted
    var filenames = '';
    for (var i = 0; i < files.length; i++){
        filenames += files[i].name + '\n';
    }
    var text = '\
==================================================\n\
Job #: ' + JobNumber + ' | Client: ' + clientName + ' | Bag: ' + bagNumber+ ' \n\
Date: ' + utils.currentDate() + ' | Time: ' + utils.currentTime() + ' \n\
Operator: ' + operator + ' \n\
\n\
Files Submitted: \n\
'+ filenames + '\n\
Notes: \n\
' + formatTextLinesMaxWidth(40, notes) + '\n\
==================================================\n\
';
    stream.write(text + "\n");
    stream.end()
}

// This function creates line items every "X" Chars
function formatTextLinesMaxWidth(max, text) {
    var reg = new RegExp("^(.{" + max + "}[^\\s]+\\s)", "mg");
    var treg = new RegExp("^.{" + max + "}[^\\s]+\\s.+$", "mg");
    var temp = text.replace(reg, "$1\n");
    var arr, finished;
    do {
        finished = true;
        arr = temp.split("\n");
        for (var i = 0; i < arr.length; i++) {
            if (treg.test(arr[i])) {
                arr[i] = arr[i].replace(reg, "$1\n");
                finished = false;
            }
        }
        temp = arr.join("\n");
    }
    while (!finished);
    return temp;
}

module.exports = {writeProductionNote};