

function flowInfo(flow, idType) {
    // Enter attributes as strings:
    // EX: flowInfo("POSTFLIGHT", "flowId")
    var flowData = {
        POSTFLIGHT: { flowId: 46, objectId: 286 },
        SubmitFileTest: { flowId: 7, objectId: 18 },
        LAUNCH: {flowId: 46, objectId: 6}
    };
    return flowData[flow][idType];
}

function sendRequest(data) {
    const xhr = new XMLHttpRequest();
    xhr.open("POST", "http://enfocusVM:51088/api/v1/job", true);
    xhr.setRequestHeader("Authorization", "Bearer " + IOGLOBALS["authBearer"]);

    xhr.onreadystatechange = function() {
        console.log('test');
        if(xhr.readyState === 4 && xhr.status === "success") {
            console.log('test');
            alert(xhr.responseText);
        }
    };
    console.log(data);
    console.log("Sending Files");
    xhr.send(data);
}

module.exports = {flowInfo,sendRequest};


