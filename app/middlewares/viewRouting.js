var fs = require('fs');
var path = require('path');

var View = function(viewName) {
    var templatePath = path.join(__dirname, "../views", viewName + ".html");
    var source = fs.readFileSync(templatePath, "utf-8");

    this.Html = source;
};

module.exports = View;