'use strict'
const electron = require('electron');
const { app, BrowserWindow, session, Menu } = electron;
const path = require('path');
const url = require('url');

let ENV = process.env.NODE_ENV;




if (ENV === 'development'){
    // Whenever any file that works with the window changes and saves
    // this will reload the window and show updates
    require('electron-reload')(__dirname);
}


// Keeping a global reference of the window object so window doesn't close
let mainWindow;

function createWindow() {


    if (process.platform === 'darwin'){
        // Create the browser window
        // FOR MAC
        mainWindow = new BrowserWindow({
            width: 1200,
            height: 600,
            resizable: false,
        });
    } else {
        // Create the browser window
        // FOR WINDOWS
        mainWindow = new BrowserWindow({
            width: 1206,
            height: 645,
            resizable: false,
        });
    }


    // load the index.html of the app (template for app)
    mainWindow.loadURL(url.format({
        pathname: path.join(__dirname, 'app/views/index.html'),
        protocol: 'file',
        slashes: true
    }));




    if (ENV === 'development'){
        // Open [DevTools]
        mainWindow.webContents.openDevTools();
    }

    // Emitted when the window is closed.
    mainWindow.on('closed', function() {
        mainWindow = null;
    });

    // Menu, Copy, Paste, Cut, Select All, etc...
    var template = [{
        label: "Application",
        submenu: [
            { label: "About Application", selector: "orderFrontStandardAboutPanel:" },
            { type: "separator" },
            { label: "Quit", accelerator: "Command+Q", click: function() { app.quit(); }}
        ]}, {
        label: "Edit",
        submenu: [
            { label: "Undo", accelerator: "CmdOrCtrl+Z", selector: "undo:" },
            { label: "Redo", accelerator: "Shift+CmdOrCtrl+Z", selector: "redo:" },
            { type: "separator" },
            { label: "Cut", accelerator: "CmdOrCtrl+X", selector: "cut:" },
            { label: "Copy", accelerator: "CmdOrCtrl+C", selector: "copy:" },
            { label: "Paste", accelerator: "CmdOrCtrl+V", selector: "paste:" },
            { label: "Select All", accelerator: "CmdOrCtrl+A", selector: "selectAll:" }
        ]}
    ];

    Menu.setApplicationMenu(Menu.buildFromTemplate(template));

}

// This method will be called when Electron has finished
// initialization and is ready to create browser windows.
// Some APIs can only be used after this event occurs.
app.on('ready', createWindow);

// Quit when all windows are closed
app.on('window-all-closed', function() {
    // On OS X it is common for the applications and their menu bar
    // to stay active until the user quits explicitly with Cmd + Q
    if (process.platform !== 'darwin') {
        app.quit();
    }
})

app.on('activate', function() {
    // On OS X it's common to re-create a window in the app when the 
    // doc icons is clicked and there are no other windows open.
    if (mainWindow === null) {
        createWindow();
    }
})

// Before quit, purge all cookies
// TODO: Need to have this send the info of action 4 to database for user quitting.
// Learn callbacks, yo!
app.on('before-quit', function() {
    session.defaultSession.clearStorageData([], function (data) {
        console.log(data);
    });
    console.log("Removed all cookies & now quitting");
});

